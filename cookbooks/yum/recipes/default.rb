#
# Cookbook Name:: yum
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

script "yum_update" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    yum update -y
  EOH
end

execute "language_change" do
  command <<-EOL
  	 localectl set-locale LANG=en_US.UTF-8
  EOL
  action :run
end

execute "stop_firewall" do
  command <<-EOL
  	 systemctl disable firewalld
  EOL
  command <<-EOL
     systemctl stop firewalld
  EOL
  action :run
end
