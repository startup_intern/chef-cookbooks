

remote_file "#{Chef::Config[:file_cache_path]}/epel-release-latest-7.noarch.rpm" do
  source 'https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm'
  action :create
end
rpm_package "epel-release-latest-7.noarch" do
  source "#{Chef::Config[:file_cache_path]}/epel-release-latest-7.noarch.rpm"
  action :install
end

remote_file "#{Chef::Config[:file_cache_path]}/webtatic-release.rpm" do
  source 'https://mirror.webtatic.com/yum/el7/webtatic-release.rpm'
  action :create
end
rpm_package "webtatic-release" do
  source "#{Chef::Config[:file_cache_path]}/webtatic-release.rpm"
  action :install
end
