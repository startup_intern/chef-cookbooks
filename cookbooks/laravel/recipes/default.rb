
# execute "download_laravel_by_composer" do
#   command <<-EOL
#   	su vagrant -l -c 'composer global require "laravel/installer"'
#   EOL
#   action :run
# end

# execute "install_laravel" do
#   command <<-EOL
#   	su vagrant -l -c 'cd /var/www/html/ && composer create-project --prefer-dist laravel/laravel startup_intern'
#   EOL
#   action :run
# end

execute "install_laravel" do
  command <<-EOL
    su vagrant -l -c 'cd /var/www/html/startup_intern && composer install'
  EOL
  action :run
end


template "/etc/php.d/xdebug.ini" do
  owner "root"
  group "root"
  mode "0644"
end
