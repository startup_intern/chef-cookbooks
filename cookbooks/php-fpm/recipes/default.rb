
template "/etc/php-fpm.d/www.conf" do
  owner "root"
  group "root"
  mode "0644"
end

execute "start_php-fpm" do
  command <<-EOL
  	systemctl start php-fpm
  EOL
  command <<-EOL
  	systemctl enable php-fpm
  EOL
  action :run
end