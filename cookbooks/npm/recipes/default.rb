#
# Cookbook Name:: npm
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
package "npm" do
  action :install
end

# execute "make js dir" do
#   command <<-EOL
#     su vagrant -l -c 'mkdir /var/www/html/startup_intern/public/js/'
#   EOL
#   action :run
# end

# %w{
# .babelrc
# .eslintrc
# package.json
# webpack.config.js
# }.each do |file|
#     template "/var/www/html/startup_intern/public/#{file}" do
#       source file
#     end
# end

execute "install react" do
  command <<-EOL
    su vagrant -l -c 'cd /var/www/html/startup_intern/public/ && npm install'
  EOL
end